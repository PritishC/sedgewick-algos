public class UF
{
    private UnionFind uf_obj;
    public UF(int N)
    {
        uf_obj = new UnionFind(N);
    }
    
    public boolean connected(int p, int q)
    {
        return uf_obj.connected(p, q);
    }
    
    public void union(int p, int q)
    {
        uf_obj.union(p, q);
    }
    
    public static void main(String... args)
    {
        int N = StdIn.readInt();
        UF uf = new UF(N);
        while(!StdIn.isEmpty())
        {
            int p = StdIn.readInt();
            int q = StdIn.readInt();
            StdOut.println(uf.connected(p, q));
            if(!uf.connected(p, q))
            {
                uf.union(p, q);
                StdOut.println(p + "" + q);
                StdOut.println(uf.connected(p, q));
            }
        }
    }
}