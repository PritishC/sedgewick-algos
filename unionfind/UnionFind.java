class UnionFind 
{
    private int[] id;
    private byte[] sz;
    private int count;
    
    public UnionFind(int N) 
    { 
        if(N < 0)
            throw new IllegalArgumentException(N + " is lesser than zero.");
        count = N;
        id = new int[N];
        sz = new byte[N];
        for(int i = 0; i < N; i++)
        {
            id[i] = i;
            sz[i] = 0;
        }
    }
    
    public int find(int i)
    {
        if(i < 0 || i >= id.length)
            throw new IndexOutOfBoundsException("nigga wut");
        while(i != id[i])
        {
            id[i] = id[id[i]]; //Flattening of tree/path compression
            i = id[i];
        }
        return i;
    }
    
    public boolean connected(int p, int q)
    {
        return find(p) == find(q);
    }
    
    public void union(int p, int q)
    {
        int i = find(p);
        int j = find(q);
        if(i == j)
            return;
        
        if(sz[i] > sz[j])
            id[i] = j;
        else if(sz[i] < sz[j])
            id[j] = i;
        else
        {
            id[j] = i;
            sz[i]++;
        }
    }
}
