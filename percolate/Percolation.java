public class Percolation
{
    public Percolation(int N)
    {
    }
    
    public void open(int i, int j)
    {
    }
    
    public boolean isOpen(int i, int j)
    {
    }
    
    public boolean isFull(int i, int j)
    {
    }
    
    public boolean percolates()
    {
    }
    
    public static randInt(int min, int max)
    {
        Random rand = new Random();
        
        int random = rand.nextInt((max - min) + 1) + min;
        
        return random;
    }
    
    public static void main(String... args)
    {
        int N = StdIn.readInt();
        Percolation perc = new Percolation(N);
        
        while(!perc.percolates())
        {
            int i = randInt(1, N);
            int j = randInt(1, N);
            
            perc.open(i, j);
        }
    }